<?php

namespace Drupal\flag_conditional_confirm\Plugin\ActionLink;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\flag\ActionLink\ActionLinkTypeBase;
use Drupal\flag\FlagInterface;
use Drupal\flag\FlagService;
use Drupal\flag\Plugin\ActionLink\ConfirmForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Conditional Confirm Form link type.
 *
 * @ActionLinkType(
 *  id = "confirm_on_condition",
 * label = @Translation("Conditional Confirm Form"),
 * description = "Redirects user to a confirmation form if condition is met."
 * )
 */
class ConditionalConfirmForm extends ConfirmForm {

  /**
   * Flag service.
   *
   * @var \Drupal\flag\FlagService
   */
  protected $flagService;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Build a new link type instance and sets the configuration.
   *
   * @param array $configuration
   *   The configuration array with which to initialize this plugin.
   * @param string $plugin_id
   *   The ID with which to initialize this plugin.
   * @param array $plugin_definition
   *   The plugin definition array.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\flag\FlagService $flag_service
   *   The Flag service.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The Module handler service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, AccountInterface $current_user, FlagService $flag_service, ModuleHandler $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $current_user);
    $this->flagService = $flag_service;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('flag'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $options = parent::defaultConfiguration();
    $options += [
      'conditional_type' => 'flag',
    ];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $plugin_id = $this->getPluginId();

    $form['display']['settings']['link_options_' . $plugin_id]['conditional_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Confirm form shown only...'),
      '#options' => [
        'flag' => $this->t('On flagging'),
        'unflag' => $this->t('On unflagging'),
        'custom' => $this->t('Custom condition (requires code)'),
      ],
      '#description' => $this->t('On what condition the confirmation should form be shown.'),
      '#default_value' => $this->configuration['conditional_type'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getAsFlagLink(FlagInterface $flag, EntityInterface $entity, ?string $view_mode = NULL): array {

    $build = ActionLinkTypeBase::getAsFlagLink($flag, $entity, $view_mode);

    $action = $build['#action'] ?? 'flag';
    $confirmation_required = $this->isConfirmationRequired($action, $flag, $entity);

    if ($confirmation_required) {
      if ($this->configuration['form_behavior'] !== 'default') {
        $build['#attached']['library'][] = 'core/drupal.ajax';
        $build['#attributes']['class'][] = 'use-ajax';
        $build['#attributes']['data-dialog-type'] = $this->configuration['form_behavior'];
        $build['#attributes']['data-dialog-options'] = Json::encode([
          'width' => 'auto',
        ]);
      }
    }
    else {
      $build['#attached']['library'][] = 'flag/flag.link_ajax';
      $build['#attributes']['class'][] = 'use-ajax';
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl($action, FlagInterface $flag, EntityInterface $entity) {

    $confirmation_required = $this->isConfirmationRequired($action, $flag, $entity);

    switch ($action) {

      case 'flag':
        $route = ($confirmation_required) ? 'flag.confirm_flag' : 'flag.action_link_flag';
        return Url::fromRoute($route, [
          'flag' => $flag->id(),
          'entity_id' => $entity->id(),
        ]);

      default:
        $route = ($confirmation_required) ? 'flag.confirm_unflag' : 'flag.action_link_unflag';
        return Url::fromRoute($route, [
          'flag' => $flag->id(),
          'entity_id' => $entity->id(),
        ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isConfirmationRequired($action, FlagInterface $flag, EntityInterface $entity) {

    switch ($this->configuration['conditional_type']) {
      case 'flag':
      case 'unflag':
        return ($action == $this->configuration['conditional_type']);
    }

    // Custom conditional required.
    /** @var \Drupal\flag\FlaggingInterface $flagging */
    $flagging = $this->flagService->getFlagging($flag, $entity);

    $result = FALSE;
    $current_user = $this->currentUser;
    $hook = 'flag_conditional_confirm_confirmation_required';
    $this->moduleHandler->invokeAllWith($hook, function (callable $hook, string $module) use (&$result, $action, $flag, $entity, $flagging, $current_user) {
      if ($result) {
        // If one implementation requires confirmation, that's enough.
        return TRUE;
      }
      $result = $hook($action,
        $flag,
        $entity,
        $flagging,
        $current_user);
      return $result;
    });

    return $result;

  }

}
